package it.itisavogadro.esercizi4a.pubblico.proveEnum;

public class TestColore {

	public static void main(String[] args) {
		Colore c1;
		ColoreConValore c2;

		c1 = Colore.RED;
		c2 = ColoreConValore.RED;

		System.out.println("c1="+c1);
		System.out.println("c1="+c1+" valore="+c2.getValue());
	}
	
}
