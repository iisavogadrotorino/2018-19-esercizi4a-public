package it.itisavogadro.esercizi4a.pubblico.proveEnum;

public enum ColoreConValore {
	RED(1), GREEN(2), ORANGE(3);

	private int posizione;
	private ColoreConValore(int pos) {
		posizione = pos;
	}
	
	public int getValue() {
		return posizione;
	}
}
