package it.itisavogadro.esercizi4a.pubblico.compitoInClasse1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class EsercizioRettangoli {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.println("Esercizio Rettangoli");
		
		Rettangolo ra = new Rettangolo(3,4,null,null,null);
		Rettangolo rb = new Rettangolo(10,4,Colore.ROSSO,null,null);
		Rettangolo rc = new Rettangolo(3,6,Colore.BEIGE,Colore.BLUE,5);
		
		System.out.println("Rettangoli: "+ra+rb+rc);
		
		List<Rettangolo> listaRettangoli = new VettoreList(2);
		
		listaRettangoli.add(ra);
		listaRettangoli.add(rb);
		listaRettangoli.add(rc);
		
		while(true) {
			System.out.println("Dati del rettangolo:");
			System.out.print("base:");
			int base = Integer.parseInt(reader.nextLine());
			System.out.print("altezza:");
			int altezza = Integer.parseInt(reader.nextLine());
			Rettangolo r = new Rettangolo(base, altezza, Colore.VERDE, Colore.ROSSO, 1);
			
			listaRettangoli.add(r);
			
			String risposta;
			while (true) {
				System.out.print("Vuoi aggiungere rettangoli ? (S/N) ");
				risposta = reader.nextLine().toUpperCase();
				if (risposta.equalsIgnoreCase("S")
				||  risposta.equalsIgnoreCase("N"))
					break;
			}
			if (risposta.equalsIgnoreCase("N")) {
				break;
			}
		}
		
		for (int i = 0; i < listaRettangoli.size(); i++) {
			System.out.println("Rettangolo:"+listaRettangoli.get(i));
		}

	}
}
