package it.itisavogadro.esercizi4a.pubblico.compitoInClasse1;


public class Rettangolo extends Quadrilatero {
	private static int contatore = 1;
	private int posizione;
	private float base;
	private float altezza;

	public float getBase() {
		return base;
	}

	public void setBase(float base) {
		if (base < 0) {
			throw new RuntimeException("No base negativa");
		}
		this.base = base;
	}

	public float getAltezza() {
		return altezza;
	}

	public void setAltezza(float altezza) {
		if (altezza < 0) {
			throw new RuntimeException("No altezza negativa");
		}
		this.altezza = altezza;
	}
	
	
	
	public Rettangolo(
					float base,
					float altezza,
					Colore coloreSuperficie,
					Colore coloreBordo,
					Integer spessoreBordo
	) {
		posizione = contatore;
		contatore++;
		setBase(base);
		setAltezza(altezza);
		if (coloreSuperficie == null) {
			coloreSuperficie = Colore.BIANCO;
		}
		setColoreSuperficie(coloreSuperficie);
		if (coloreBordo == null) {
			coloreBordo = Colore.NERO;
		}
		setColoreBordo(coloreBordo);
		if (spessoreBordo == null) {
			setSpessoreBordo(1);
		} else {
			setSpessoreBordo(spessoreBordo);
		}
	}

	public int getPosizione() {
		return posizione;
	}

	private void setPosizione(int posizione) {
		throw new RuntimeException("USO VIETATO");
	}

	@Override
	float getArea() {
		return base * altezza;
	}

	@Override
	public String toString() {
		return "\nRettangolo {" + "pos=" + posizione + "°, base=" + base
				+ ", altezza=" + altezza
				+ ", superficie=" + getColoreSuperficie().name()
				+ ", bordo=" + getColoreBordo().name()
				+ ", spessore=" + getSpessoreBordo()
				+ '}';
	}	
}
