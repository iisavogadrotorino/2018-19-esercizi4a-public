package it.itisavogadro.esercizi4a.pubblico.compitoInClasse1;

public abstract class Quadrilatero extends Poligono {
	public Quadrilatero() {
		super.setNumeroLati(4);
	}

	@Override
	protected void setNumeroLati(int numeroLati) {
		throw new RuntimeException("USO VIETATO");
	}
	
	
}
