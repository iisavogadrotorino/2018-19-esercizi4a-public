package it.itisavogadro.esercizi4a.pubblico.compitoInClasse1;

public abstract class Poligono {
	private int numeroLati;
	private Colore coloreSuperficie;
	private Colore coloreBordo;
	private int spessoreBordo;

	public int getNumeroLati() {
		return numeroLati;
	}

	protected void setNumeroLati(int numeroLati) {
		if (numeroLati < 3) {
			throw new RuntimeException("Il poligono deve avere almeno 3 lati");
		}
		this.numeroLati = numeroLati;
	}

	public Colore getColoreSuperficie() {
		return coloreSuperficie;
	}

	public void setColoreSuperficie(Colore coloreSuperficie) {
		this.coloreSuperficie = coloreSuperficie;
	}

	public Colore getColoreBordo() {
		return coloreBordo;
	}

	public void setColoreBordo(Colore coloreBordo) {
		this.coloreBordo = coloreBordo;
	}

	public int getSpessoreBordo() {
		return spessoreBordo;
	}

	public void setSpessoreBordo(int spessoreBordo) {
		if (spessoreBordo < 0) {
			throw new RuntimeException("Il bordo non può avere spessore negativo");
		}
		this.spessoreBordo = spessoreBordo;
	}
	
	abstract float getArea();
}
