package it.itisavogadro.esercizi4a.pubblico.reflection;

import it.itisavogadro.esercizi4a.pubblico.gestionePila.Studente;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class TestReflection {
	private static final int COSTANTE = 0;

	public static void main(String[] args) throws Exception {
		Studente s1 = new Studente("Rossi", "Mario");
		
		Method[] metodi = Studente.class.getDeclaredMethods();
		Field[]  campi  = Studente.class.getDeclaredFields();
		
		System.out.println("Campi della classe "+Studente.class.getName());
		for (Field campo : campi) {
			System.out.println(campo.getName()+": modificatori="+campo.getModifiers());
			if ((Modifier.PUBLIC & campo.getModifiers()) != 0) {
				System.out.println("PUBLIC");
			}
			if ((Modifier.PRIVATE & campo.getModifiers()) != 0) {
				System.out.println("PRIVATE");
			}
			if ((Modifier.STATIC & campo.getModifiers()) != 0) {
				System.out.println("STATIC");
			}
		}
		
		System.out.println("Metodi della classe "+Studente.class.getName());
		for (Method metodo : metodi) {
			System.out.println(metodo.getName()+": modificatori="+metodo.getModifiers());
			if ((Modifier.PUBLIC & metodo.getModifiers()) != 0) {
				System.out.println("PUBLIC");
			}
			if ((Modifier.PRIVATE & metodo.getModifiers()) != 0) {
				System.out.println("PRIVATE");
			}
			if ((Modifier.STATIC & metodo.getModifiers()) != 0) {
				System.out.println("STATIC");
			}
		}
		
		
		System.out.println("MODIFICA DI UNA COSTANTE MEDIANTE RFLECTION");
		// cambiare il valore di una costante "final" è inutile perché il valore della costante
		// viene già usato in fase di compilazione e non di runtime
		// riferimento:   https://caffinc.github.io/2015/12/static-final-java-junit-test/
		
		// COSTANTE = 10;
		System.out.println("COSTANTE = "+COSTANTE);
		Field varCOSTANTE = TestReflection.class.getDeclaredField("COSTANTE");
		varCOSTANTE.setAccessible(true);
		//varCOSTANTE.setInt(null, 10);
		int modifier = varCOSTANTE.getModifiers();
		System.out.println("COSTANTE.modifier = "+modifier);
		
		Field modifiersField = Field.class.getDeclaredField( "modifiers" );
		boolean isModifierAccessible = modifiersField.isAccessible();
		modifiersField.setAccessible( true );
		modifiersField.setInt( varCOSTANTE, varCOSTANTE.getModifiers() & ~Modifier.FINAL );

		varCOSTANTE.setAccessible(true);
		varCOSTANTE.setInt(null, 10);
		System.out.println("COSTANTE = "+COSTANTE);
		System.out.println("COSTANTE = "+varCOSTANTE.getInt(null));
	}	
}
