package it.itisavogadro.esercizi4a.pubblico.gestionePila;

import it.itisavogadro.esercizi4a.pubblico.compitoInClasse1.Colore;
import it.itisavogadro.esercizi4a.pubblico.compitoInClasse1.Rettangolo;


public class GestionePila {

	public static void main(String[] args) {
		Rettangolo r1 = new Rettangolo(1.0f,10.0f,Colore.BIANCO,null,2);
		Stack<Studente> pila1 = new Stack<Studente>();
		Studente s1 = new Studente("Rossi", "Mario");
		pila1.push(s1);
		s1 = new Studente("Verdi", "Franco");
		pila1.push(s1);
		s1 = new Studente("Piscopo", "Luca");
		pila1.push(s1);
		
		//pila1.push(r1);
		
		
		pila1.controllaContenuto();
		
		System.out.println("Estrazione:");
		do {
			s1 = pila1.pop();
			System.out.println("estratto "+s1);
		} while (s1 != null);
	}
	
}
