package it.itisavogadro.esercizi4a.pubblico.gestionePila;

public class Studente {
	private static int contatore = 0;
	private String cognome;
	private String nome;

	public static int getContatore() {
		return contatore;
	}

	private static void setContatore(int contatore) {
		Studente.contatore = contatore;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Studente(String cognome, String nome) {
		this.cognome = cognome;
		this.nome = nome;
	}
	
	public static String upperCase(Studente s) {
		return s.getCognome().toUpperCase()+" "+s.getNome().toUpperCase();
	}

	@Override
	public String toString() {
		return "Studente{" + "cognome=" + cognome + ", nome=" + nome + '}';
	}	
}
