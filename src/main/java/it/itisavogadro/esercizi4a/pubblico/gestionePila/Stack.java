package it.itisavogadro.esercizi4a.pubblico.gestionePila;

public class Stack<Tipo> {

	StackElement first;

	Stack() {
		first = null;
	}

	public void push(Tipo data) {
		StackElement e = new StackElement(data);

		if (first != null) {
			e.next = first;
		}
		first = e;
	}

	public Tipo pop() {
		if (first == null) {
			return null;
		}
		StackElement e = first;
		first = first.next;

		return e.data;
	}

	public void controllaContenuto(){
		StackElement e = first;
		while (e != null) {
			System.out.println("elemento "+e.data);
			e = e.next;
		}
	}
	
	class StackElement {

		Tipo data;
		StackElement next;

		StackElement(Tipo data) {
			this.data = data;
			this.next = null;
		}
	}
}
