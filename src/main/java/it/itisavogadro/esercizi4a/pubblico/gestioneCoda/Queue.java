package it.itisavogadro.esercizi4a.pubblico.gestioneCoda;

public class Queue<Tipo> {

	QueueElement first;
	QueueElement last;

	Queue() {
		first = null;
		last = null;
	}

	public void push(Tipo data) {
		QueueElement e = new QueueElement(data);

		if (first != null) {
			last.next = e;
		} else {
			first = e;
		}
		last = e;
	}

	public Tipo pop() {
		if (first == null) {
			return null;
		}
		QueueElement e = first;
		first = first.next;
		
		if (first == null) {
			last = null;
		}

		return e.data;
	}

	public void controllaContenuto(){
		QueueElement e = first;
		while (e != null) {
			System.out.println("elemento "+e.data);
			e = e.next;
		}
	}
	
	private class QueueElement {

		Tipo data;
		QueueElement next;

		QueueElement(Tipo data) {
			this.data = data;
			this.next = null;
		}
	}
}
