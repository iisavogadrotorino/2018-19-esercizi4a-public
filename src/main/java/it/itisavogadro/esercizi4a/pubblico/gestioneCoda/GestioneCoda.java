package it.itisavogadro.esercizi4a.pubblico.gestioneCoda;


public class GestioneCoda {

	public static void main(String[] args) {
		String colore = "ROSSO";
		Queue<String> coda1 = new Queue<String>();
		coda1.push("ROSSO");
		System.out.println(coda1.pop());
		coda1.push("BIANCO");
		coda1.push("VERDE");
		coda1.push("VIOLA");
		System.out.println(coda1.pop());
		coda1.push("ROSSO");
		System.out.println(coda1.pop());
		coda1.push("NERO");
		coda1.push("BLU");
		coda1.push("GIALLO");
		coda1.push("ROSA");
		System.out.println(coda1.pop());
		System.out.println(coda1.pop());

		coda1.controllaContenuto();
		
		System.out.println("Estrazione:");
		do {
			colore = coda1.pop();
			System.out.println("estratto "+colore);
		} while (colore != null);
	}
	
}
